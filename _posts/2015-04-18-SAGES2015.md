---
layout: page
header: no
subheadline: "SAGES"
title:  "HERos at SAGES 2015"
teaser: "HERo Lab presented at SAGES 2015 in Nashville, Tennessee as part of the Emerging Technologies session."
meta_teaser:
breadcrumb: true
categories:
    - conferences
tags:
author:
---
*Feeling Responsive* shows metadata by default. The default behaviour can be changed via `config.yml`. To show metadata at the end of a page/post just add the following to front matter:
