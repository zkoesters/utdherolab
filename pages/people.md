---
layout: page
show_meta: false
title: "People"
subheadline:
teaser:
header: no
image:
    title: "herolab_group.jpg"
    caption: "HERo Lab Group, Spring 2015"
permalink: "/people/"
---
## Principal Investigator

### Ann Majewicz

<div class="row">
  <div class="large-4 columns">
      <img src="{{ site.url }}/images/ann_majewicz.png" alt="Ann Majewicz" width="150">
  </div>
  <div class="large-8 columns">
    <p>Assistant Professor, Department of Mechanical Engineering</p>
    <p>Adjunct Assistant Professor, Department of Surgery, UT Southwestern Medical Center</p>

    <p><b>Education:</b></p>
    <ul>
        <li>Ph.D., Mechanical Engineering, Stanford University</li>
        <li>M.S, Mechanical Engineering, Johns Hopkins University</li>
        <li>B.S., Mechanical Engineering, University of St. Thomas</li>
        <li>B.S., Electrical Engineering, University of St. Thomas</li>
    </ul>
    <p><b>Email:</b></p>
    <p><a href="mailto:ann.majewicz@utdallas.edu" target="_top">ann.majewicz@utdallas.edu</a></p>
  </div>
</div>

## Ph.D. Students

### Marzieh Ershad

<div class="row">
  <div class="large-4 columns">
        <img src="{{ site.url }}/images/marzieh_ershad.png" alt="Marzieh Ershad" width="150">
    </div>
  <div class="large-8 columns">
    <p><b>Project:</b></p>
    <p>Semantic Labeling of Surgical Skill</p>

    <p><b>Education:</b></p>
    <ul>
        <li>M.S, Biomedical Engineering, Tehran University of Medical Sciences</li>
        <li>B.S., Electrical Engineering, Shiraz University</li>
    </ul>
    <p><b>Email:</b></p>
    <p><a href="mailto:marzieh.ershadlangroodi@utdallas.edu" target="_top">marzieh.ershadlangroodi@utdallas.edu</a></p>
  </div>
</div>

### Zachary Koesters

<div class="row">
  <div class="large-4 columns">
      <img src="{{ site.url }}/images/zachary_koesters.png" alt="Zachary Koesters" width="150">
  </div>
  <div class="large-8 columns">
    <p><b>Project:</b></p>
    <p>Guidance for Percutaneous Needle Insertion</p>

    <p><b>Education:</b></p>
    <ul>
        <li>B.S., Mechanical Engineering, University of Texas at Dallas</li>
    </ul>
    <p><b>Email:</b></p>
    <p><a href="mailto:zkoesters@utdallas.edu" target="_top">zkoesters@utdallas.edu</a></p>
  </div>
  </div>

### Meenakshi Narayan

<div class="row">
  <div class="large-4 columns">
        <img src="{{ site.url }}/images/meena_narayan.png" alt="Meenakshi Narayan" width="150">
    </div>
  <div class="large-8 columns">
    <p><b>Project:</b></p>
    <p>Guidance for Percutaneous Needle Insertion</p>

    <p><b>Education:</b></p>
    <ul>
        <li>B.S., Mechanical Engineering, University of Texas at Dallas</li>
    </ul>
    <p><b>Email:</b></p>
    <p><a href="mailto:meenkshi.narayan@utdallas.edu" target="_top">meenakshi.narayan@utdallas.edu</a></p>
  </div>
</div>

## Undergraduate Students

### Husam Wadi

<div class="row">
  <div class="large-4 columns">
      <img src="{{ site.url }}/images/husam_wadi.jpg" alt="Husam Wadi" width="150">
  </div>
  <div class="large-8 columns">
    <p><b>Project:</b></p>
    <p>Constraint Mechanism for Improved Laparoscopic Tool Handling</p>

    <p><b>Education:</b></p>
    <ul>
        <li>B.S., Mechanical Engineering, University of Texas at Dallas (in progress)</li>
    </ul>
    <p><b>Email:</b></p>
    <p><a href="mailto:husam.wadi@utdallas.edu" target="_top">husam.wadi@utdallas.edu</a></p>
  </div>
</div>

### Keene Chin

<div class="row">
  <div class="large-4 columns">
      <img src="{{ site.url }}/images/keene_chin.png" alt="Keene Chin" width="150">
  </div>
  <div class="large-8 columns">
    <p><b>Project:</b></p>
    <p>Stress Inoculation Training for Trauma Surgeons</p>

    <p><b>Education:</b></p>
    <ul>
        <li>B.S., Mechanical Engineering, University of Texas at Dallas (in progress)</li>
    </ul>
    <p><b>Email:</b></p>
    <p><a href="mailto:keene.chin@utdallas.edu" target="_top">keene.chin@utdallas.edu</a></p>
  </div>
</div>

### Thomas Amlee

<div class="row">
  <div class="large-4 columns">
      <img src="{{ site.url }}/images/thomas_amlee.png" alt="Thomas Amlee" width="150">
  </div>
  <div class="large-8 columns">
    <p><b>Project:</b></p>
    <p>Constraint Mechanism for Improved Laparoscopic Tool Handling</p>

    <p><b>Education:</b></p>
    <ul>
        <li>B.S., Electrical Engineering, University of Texas at Dallas (in progress)</li>
    </ul>
    <p><b>Email:</b></p>
    <p><a href="mailto:thomas.amlee@utdallas.edu" target="_top">thomas.amlee@utdallas.edu</a></p>
  </div>
</div>

### Isabella Reed

<div class="row">
  <div class="large-4 columns">
      <img src="{{ site.url }}/images/isabella_reed.png" alt="Isabella Reed" width="150">
  </div>
  <div class="large-8 columns">
    <p><b>Project:</b></p>
    <p></p>

    <p><b>Education:</b></p>
    <ul>
        <li>B.S., Biomedical Engineering, University of Texas at Dallas (in progress)</li>
    </ul>
  </div>
</div>
