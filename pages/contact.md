---
layout: page
show_meta: false
title: "Contact"
meta_title:
subheadline:
teaser:
header: no
permalink: "/contact/"
---
The HeRo Lab is located in the Engineering and Computer Science North Building (ECSN) at the University of Texas at Dallas. Our lab, ECSN 2.412, can be found in the northeast hallway of the building.

Lab Telephone: (972)-883-6559

Dr. Ann Majewicz’s office is also in ECSN, 2.7. Office Telephone: (972)-883-4660.
