---
layout: page
title: "About"
subheadline:
teaser:
permalink: "/info/"
header: no
---
The Human-Enabaled Robotic Technology Lab aims to greatly improve healthcare by synthesizing information, sharing knowledge, and assisting with the delivery of care. Our lab aims to translate effective robotic technology into clinical use, by focusing on novel electro- mechanical systems, intuitive teleoperation strategies, and human-robot sensory interactions (e.g. haptic, visual, auditory) for medical intervention, simulation, and training. We work closely with surgeons from the UTSW Department of Surgery, the Southwestern Center for Minimally Invasive Surgery, and Children’s Hospital in Dallas.

The HeRo Lab is directed by Dr. Ann Majewicz and is part of the Dynamics Systems and Controls group in the Department of Mechanical Engineering at the University of Texas at Dallas.

The Human-Enabaled Robotic Technology Lab aims to greatly improve healthcare by synthesizing information, sharing knowledge, and assisting with the delivery of care. Our lab aims to translate effective robotic technology into clinical use, by focusing on novel electro- mechanical systems, intuitive teleoperation strategies, and human-robot sensory interactions (e.g. haptic, visual, auditory) for medical intervention, simulation, and training. We work closely with surgeons from the UTSW Department of Surgery, the Southwestern Center for Minimally Invasive Surgery, and Children’s Hospital in Dallas.

The HeRo Lab is directed by Dr. Ann Majewicz and is part of the Dynamics Systems and Controls group in the Department of Mechanical Engineering at the University of Texas at Dallas.
