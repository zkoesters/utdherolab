---
#
# Use the widgets beneath and the content will be
# inserted automagically in the webpage. To make
# this work, you have to use › layout: frontpage
#
layout: frontpage
header:
  title: "Human-enabled Robotics Lab"
  image_fullwidth: "splash.png"
widget1:
  title: "Recent News"
  url: '/news/'
  text: 'HERo Lab presented at ICRA 2015 in Seattle, Washington as part of the Late Breaking Results session.'
widget2:
  title: "Recent Papers and Talks"
  url: '/publications/'
  text: 'Coming Soon.'
widget3:
  title: "Upcoming"
  text: 'Coming Soon.'
permalink: /index.html
---
